<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.guerlab</groupId>
    <artifactId>guerlab-spring</artifactId>
    <version>2.0.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>${project.groupId}:${project.artifactId}</name>
    <description>guerlab spring extension</description>
    <url>https://gitee.com/guerlab_net/guerlab-spring</url>

    <licenses>
        <license>
            <name>GNU LESSER GENERAL PUBLIC LICENSE Version 3</name>
            <url>https://www.gnu.org/licenses/lgpl-3.0.txt</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <id>guer</id>
            <name>guer</name>
            <email>master@guerlab.net</email>
            <organization>guerlab</organization>
            <organizationUrl>http://www.guerlab.net</organizationUrl>
        </developer>
    </developers>

    <organization>
        <name>guerlab</name>
        <url>http://www.guerlab.net</url>
    </organization>

    <scm>
        <connection>scm:git:https://gitee.com/guerlab_net/guerlab-spring.git</connection>
        <developerConnection>scm:git:https://gitee.com/guerlab_net/guerlab-spring.git</developerConnection>
        <url>https://gitee.com/guerlab_net/guerlab-spring</url>
        <tag>HEAD</tag>
    </scm>

    <issueManagement>
        <system>gitee</system>
        <url>https://gitee.com/guerlab_net/guerlab-spring/issues</url>
    </issueManagement>

    <properties>
        <java.version>1.8</java.version>
        <maven-gpg-plugin.version>1.6</maven-gpg-plugin.version>
        <plugin.javadoc.version>2.10.2</plugin.javadoc.version>
        <plugin.deploy.version>2.8.2</plugin.deploy.version>
        <project.encoding>UTF-8</project.encoding>

        <guerlab-commons.version>1.4.0</guerlab-commons.version>
        <guerlab-web.version>1.4.0</guerlab-web.version>

        <guerlab-spring.version>2.0.0-SNAPSHOT</guerlab-spring.version>

        <spring-cloud.version>Finchley.RC1</spring-cloud.version>
        <spring-boot.version>2.0.1.RELEASE</spring-boot.version>

        <swagger.version>1.5.18</swagger.version>
        <springfox.version>2.8.0</springfox.version>

        <druid.version>1.1.9</druid.version>
        <mybatis-starter.version>1.3.2</mybatis-starter.version>
        <mybatis-typehandlers-jsr310.version>1.0.2</mybatis-typehandlers-jsr310.version>

        <persistence-api.version>1.0.2</persistence-api.version>

        <pagehelper-spring-boot-starter.version>1.2.5</pagehelper-spring-boot-starter.version>
        <pagehelper.version>5.1.4</pagehelper.version>

        <mapper-spring-boot-starter.version>2.0.2</mapper-spring-boot-starter.version>
        <mapper.version>1.0.2</mapper.version>

        <jjwt.version>0.9.0</jjwt.version>
        
        <lombok.version>1.16.20</lombok.version>
    </properties>

    <repositories>
        <repository>
            <id>spring-milestones</id>
            <name>Spring Milestones</name>
            <url>https://repo.spring.io/libs-milestone</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-parent</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-commons</artifactId>
                <version>${guerlab-commons.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-web</artifactId>
                <version>${guerlab-web.version}</version>
            </dependency>

            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-commons</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-cloud-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-mapper-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-mybatis-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-mysql-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-redis-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-searchparams</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-swagger2-cloud-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-swagger2-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-swagger2-ui-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-task-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-upload-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab</groupId>
                <artifactId>guerlab-spring-upload-aliyun-oss-starter</artifactId>
                <version>${guerlab-spring.version}</version>
            </dependency>

            <!-- swagger -->
            <dependency>
                <groupId>io.swagger</groupId>
                <artifactId>swagger-annotations</artifactId>
                <version>${swagger.version}</version>
            </dependency>
            <dependency>
                <groupId>io.springfox</groupId>
                <artifactId>springfox-swagger2</artifactId>
                <version>${springfox.version}</version>
            </dependency>
            <dependency>
                <groupId>io.springfox</groupId>
                <artifactId>springfox-swagger-ui</artifactId>
                <version>${springfox.version}</version>
            </dependency>

            <!-- db -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid-spring-boot-starter</artifactId>
                <version>${druid.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mybatis.spring.boot</groupId>
                <artifactId>mybatis-spring-boot-starter</artifactId>
                <version>${mybatis-starter.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mybatis</groupId>
                <artifactId>mybatis-typehandlers-jsr310</artifactId>
                <version>${mybatis-typehandlers-jsr310.version}</version>
            </dependency>

            <dependency>
                <groupId>tk.mybatis</groupId>
                <artifactId>mapper-spring-boot-starter</artifactId>
                <version>${mapper-spring-boot-starter.version}</version>
            </dependency>
            <dependency>
                <groupId>tk.mybatis</groupId>
                <artifactId>mapper-core</artifactId>
                <version>${mapper.version}</version>
            </dependency>

            <dependency>
                <groupId>com.github.pagehelper</groupId>
                <artifactId>pagehelper-spring-boot-starter</artifactId>
                <version>${pagehelper-spring-boot-starter.version}</version>
            </dependency>
            <dependency>
                <groupId>com.github.pagehelper</groupId>
                <artifactId>pagehelper</artifactId>
                <version>${pagehelper.version}</version>
            </dependency>

            <dependency>
                <groupId>javax.persistence</groupId>
                <artifactId>persistence-api</artifactId>
                <version>${persistence-api.version}</version>
            </dependency>

            <!-- jwt -->
            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt</artifactId>
                <version>${jjwt.version}</version>
            </dependency>

            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <profiles>
        <profile>
            <id>guerlab</id>
            <distributionManagement>
                <snapshotRepository>
                    <id>guerlab-snapshot</id>
                    <name>guerlab Snapshot Repository</name>
                    <url>http://rep.guerlab.net/repository/guerlab-snapshot</url>
                </snapshotRepository>
                <repository>
                    <id>guerlab-release</id>
                    <name>guerlab Release Repository</name>
                    <url>http://rep.guerlab.net/repository/guerlab-release</url>
                </repository>
            </distributionManagement>
        </profile>
        <profile>
            <id>central</id>
            <distributionManagement>
                <snapshotRepository>
                    <id>sonatype-nexus-snapshots</id>
                    <name>Sonatype Nexus Snapshots</name>
                    <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
                </snapshotRepository>
                <repository>
                    <id>sonatype-nexus-staging</id>
                    <name>Nexus Release Repository</name>
                    <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
                </repository>
            </distributionManagement>
        </profile>
    </profiles>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-gpg-plugin</artifactId>
                    <version>${maven-gpg-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <executions>
                        <execution>
                            <goals>
                                <goal>integration-test</goal>
                                <goal>verify</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <source>${java.version}</source>
                        <target>${java.version}</target>
                        <encoding>${project.encoding}</encoding>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <configuration>
                        <encoding>${project.encoding}</encoding>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${plugin.javadoc.version}</version>
                    <configuration>
                        <encoding>${project.encoding}</encoding>
                        <aggregate>true</aggregate>
                        <charset>${project.encoding}</charset>
                        <docencoding>${project.encoding}</docencoding>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <modules>
        <module>guerlab-spring-commons</module>
        <module>guerlab-spring-cloud-starter</module>
        <module>guerlab-spring-task-starter</module>
        <module>guerlab-spring-mapper-starter</module>
        <module>guerlab-spring-mybatis-starter</module>
        <module>guerlab-spring-mysql-starter</module>
        <module>guerlab-spring-redis-starter</module>
        <module>guerlab-spring-searchparams</module>
        <module>guerlab-spring-swagger2-cloud-starter</module>
        <module>guerlab-spring-swagger2-starter</module>
        <module>guerlab-spring-swagger2-ui-starter</module>
        <module>guerlab-spring-upload-starter</module>
        <module>guerlab-spring-upload-aliyun-oss-starter</module>
    </modules>
</project>